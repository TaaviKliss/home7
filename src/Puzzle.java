import java.util.HashMap;
import java.util.HashSet;

public class Puzzle {

	private static String elemendid;
	private static String[] sonad;
	private static int loendur = 0;

	/**
	 * Solve the word puzzle.
	 * 
	 * @param args
	 *            three words (addend1, addend2 and sum)
	 */
	public static void main(String[] args) {

		/**
		 * Laenasin testimiseks näited PuzzleTest failist.
		 */
		// args = new String[]{ "YKS", "KAKS", "KOLM" };
		// args = new String[]{"SEND", "MORE", "MONEY"};
		 args = new String[]{"ABCDEFGHIJAB", "ABCDEFGHIJA", "ACEHJBDFGIAC"};

		/**
		 * Järgnevad tingimuslaused on laenatud siit:
		 * http://www.java-forum.org/thema/send-more-money-problem.137653/
		 * 
		 * Tegemist oli tingimuskontrollirohke koodiga, mille võtsin ka siin
		 * eeskujuks.
		 */

		if (args.length != 3) {

			throw new RuntimeException("Sisend peab koosnema ainult kolmest sõnast," + " aga Teie sisendis on neid "
					+ args.length + ". (MAIN)");
		}

		for (String sona : args) {

			if (sona.length() > 18)

				throw new RuntimeException("Teie sisendis olevas sõnes " + sona + " on üle 18 tähemärgi. (MAIN)");

			if (sona.length() <= 0)

				throw new RuntimeException("Teie sisend sisaldab tühja sõna. (MAIN)");

			for (char taht : sona.toCharArray()) {

				if (!Character.isLetter(taht))

					throw new RuntimeException(
							"Teie sisendis olev sõne " + sona + " sisaldab võõrast tähemärki. (MAIN)");
			}
		}

		/**
		 * Selline sõnade indekseerimise põhi on laenatud siit:
		 * https://www.codeproject.com/articles/176768/cryptarithmetic
		 */

		sonad = new String[3];
		sonad[0] = args[0];
		sonad[1] = args[1];
		sonad[2] = args[2];
		String sonadKokku = args[0] + args[1] + args[2];
		elemendid = "";

		for (int i = 0; i < sonadKokku.length(); i++) {

			char taht = sonadKokku.charAt(i);
			if (elemendid.indexOf(taht) < 0)
				elemendid += taht;
		}
		kombinatsiooniVorm(10, elemendid.length());
		System.out.println("Lahendeid leidus " + loendur + ". (MAIN)");
		
		// Pärast tulemuse tagastamist nullin loenduri.
		loendur = 0;
	}

	public static void kombineeri(int n, int k, HashSet<Integer> vaartusta, int[] kombinatsioon) {

		HashMap<Character, Integer> tahemapper = new HashMap<Character, Integer>();

		for (int i = 0; i < k; i++)

			tahemapper.put(elemendid.charAt(i), kombinatsioon[i]);

		long[] vastused = new long[3];

		for (int j = 0; j < 3; j++) {

			String sona = sonad[j];

			if (tahemapper.get(sona.charAt(0)) == 0)

				return;

			long vastus = 0;

			for (int i = 0; i < sona.length(); i++)

				vastus = 10 * vastus + tahemapper.get(sona.charAt(i));
			vastused[j] = vastus;
		}

		/**
		 * Sõnade liitmise koodipõhi on laenatud siit:
		 * https://coderanch.com/t/595306/java/Optimizing-send-money-code
		 * 
		 * Kuid muutsin kujunduselt lihtsamaks.
		 */
		if (vastused[0] + vastused[1] == vastused[2]) {

			System.out.println(tahemapper);
			System.out.println(sonad[0] + " + " + sonad[1] + " = " + sonad[2]);
			System.out.println(vastused[0] + " + " + vastused[1] + " = " + vastused[2]);
			System.out.println();
			loendur++;
		}
	}

	public static void kombinatsiooniVorm(int a, int b) {
		sobita(a, b, new HashSet<Integer>(), new int[b]);
	}

	public static void sobita(int a, int b, HashSet<Integer> vaartusta, int[] kombinatsioon) {
		if (vaartusta.size() == b)
			kombineeri(a, b, vaartusta, kombinatsioon);
		else {
			for (int i = 0; i < a; i++)
				if (!vaartusta.contains(i)) {
					kombinatsioon[vaartusta.size()] = i;
					vaartusta.add(i);
					sobita(a, b, vaartusta, kombinatsioon);
					vaartusta.remove(i);
				}
		}
	}

}